﻿using System;
namespace Libreium.QConfig
{
    public class Value : Item
    {
        #region Fields

        private string _value;
        private string _defaultValue;

        #endregion

        #region Properties

        public override string Val 
        {   
            get => _value ?? _defaultValue ?? ""; 
            set => _value = value; 
        }

        #endregion

        #region Private Methods

        #endregion

        #region Public Methods

        public override string GetExportText()
        {
            string textOut = Name + "=";

            textOut += _value ?? _defaultValue ?? "";

            return textOut;
        }

        public override string GetValue()
        {
            return _value ?? _defaultValue ?? "";
        }

        public override void SetDefault()
        {
            _value = _defaultValue ?? "";
        }

        public override void SetValue(string value)
        {
            _value = value;
        }

        #endregion

        #region Constructors

        public Value(string name)
        {
            Name = name ?? throw new Exception("Value Set Parameter Name Can not be Null!!");
        }

        public Value(string name, string defaultValue)
        {
            Name = name ?? throw new Exception("Value Set Parameter Name Can not be Null!!");

            _defaultValue = defaultValue;
        }

        public Value(string name, string defaultValue, string value)
        {
            Name = name ?? throw new Exception("Value Set Parameter Name Can not be Null!!");

            _defaultValue = defaultValue;

            _value = value;
        }


        #endregion
    }
}
