﻿using System;
using System.Collections.Generic;

namespace Libreium.QConfig
{
    public class Feature
    {
        #region Fields

        #endregion

        #region Properties

        public string Name { get; private set; }

        public List<Item> Items = new List<Item>();

        #endregion

        #region Privete Methods

        #endregion

        #region Public Methods

        public string GetExportText()
        {
            string textOut = "";

            textOut = $"[{Name}]\n";

            foreach (Item item in Items)
            {
                textOut += item.GetExportText() + "\n";
            }

            return textOut;
        }

        public Item Item(string name)
        {
            name = name.ToLower().Replace(" ", "");

            foreach (Item item in Items)
            {
                string currentItemName = item.Name.ToLower().Replace(" ", "");

                if (currentItemName == name)
                {
                    return item;
                }
            }

            return null;
        }

        public void SetDefault()
        {
            foreach(Item item in Items)
            {
                item.SetDefault();
            }
        }

        #endregion

        #region Constructors

        public Feature(string featureName)
        {
            Name = featureName;
        }

        public Feature(string featureName, List<Item> items)
        {
            Name = featureName;
            Items = items;
        }

        #endregion
    }
}
