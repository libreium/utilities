﻿using System;
using System.Collections.Generic;
namespace Libreium.QConfig
{
    public class StringList : Item
    {
        #region Fields

        private List<string> _defaultValues = new List<string>();

        private List<string> _values = new List<string>();

        #endregion

        #region Properties

        public List<string> Values
        {
            get => _values;
            set
            {
                _values = value;
            }
        }

        public override string Val 
        {
            get => throw new NotImplementedException();
            set => throw new NotImplementedException(); 
        }


        #endregion

        #region Private Methods

        #endregion

        #region Public Methods

        public void AddToList(string value)
        {
            _values.Add(value);
        }

        public override string GetValue()
        {
            string valueOut = "";

            if (_values != null)
            {
                foreach(string s in _values)
                {
                    valueOut += s + "\n";
                }
                return valueOut;
            }

            if (_defaultValues != null)
            {
                foreach (string s in _defaultValues)
                {
                    valueOut += s + "\n";
                }
                return valueOut;
            }

            return "";
        }

        public override void SetDefault()
        {
            _values = _defaultValues;
        }

        public override void SetValue(string value)
        {
            if (value != string.Empty)
            {
                string[] values = value.Split('\n');

                foreach (string s in values)
                {
                    Values.Add(s);
                }
            }
        }

        public override string GetExportText()
        {
            if (_values == null && _defaultValues == null)
            {
                return Name + "{}";
            }

            string textOut = Name + "={";

            List<string> values = _values ?? _defaultValues;

            for (int i = 0; i <= values.Count - 1; i++)
            {
                textOut += (i < values.Count - 1) ? values[i] + "," : values[i] ;
            }

            textOut += "}\n";

            return textOut;
        }

        #endregion

        #region Constructors

        public StringList(string name)
        {
            Name = name ?? throw new Exception("StringList Set Parameter Name Can not be Null!!");
        }

        public StringList(string name, List<string> defaultValues)
        {
            Name = name ?? throw new Exception("StringList Set Parameter Name Can not be Null!!");

            _defaultValues = defaultValues ?? _defaultValues;
        }

        public StringList(string name, List<string> defaultValues, List<string> values)
        {
            Name = name ?? throw new Exception("StringList Set Parameter Name Can not be Null!!");

            _defaultValues = defaultValues ?? _defaultValues;

            _values = values ?? _values;
        }

        #endregion

    }
}
