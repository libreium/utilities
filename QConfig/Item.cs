﻿using System;
namespace Libreium.QConfig
{
    public abstract class Item
    {
        #region Fields

        #endregion

        #region Properties

        public string Name { get; set; }

        #endregion

        #region Privete Methods

        #endregion

        #region Public Methods

        public abstract string GetValue();

        public abstract string GetExportText();

        public abstract void SetDefault();

        public abstract void SetValue(string value);

        public abstract string Val { get; set; }

        #endregion

        #region Constructors


        #endregion
    }
}
