﻿using System;
using System.IO;
namespace Libreium.Logger
{
    public class Log
    {
        #region feilds

        bool _append;

        string _filePath;

        string _logName;

        StreamWriter _logStream;

        #endregion

        #region Private Methods

        private string CurrentDateTime()
        {
            return DateTime.Now.ToString();
        }

        #endregion

        #region Public Methods

        public bool AddToLog(string lineText)
        {
            try
            {
                _logStream = new StreamWriter(_filePath, true);

                _logStream.WriteLine(CurrentDateTime() + " " + lineText);

                _logStream.Close();

                return true;
            }
            catch(Exception ex)
            {
#if DEBUG
                Console.WriteLine($"logger exception {ex.Message} log {_logName}");
#endif
                return false;
            }
        }

        #endregion

        public Log(string filePath, string logName, bool append)
        {
            _append = append;

            _filePath = filePath;

            _logName = logName;

            try
            {
                _logStream = new StreamWriter(filePath, append);

                _logStream.WriteLine($"Log {logName} started {CurrentDateTime()} ");

                _logStream.Close();
            }
            catch(Exception ex)
            {
                Console.WriteLine($"logger exception {ex.Message} log {logName}");
            }

        }
    }
}
