﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Libreium.QConfig
{
    public class Config
    {
        #region Fields

        private string _configText;

        #endregion

        #region Properties

        public string ConfigText 
        { 
            get => _configText;
            private set
            {
                _configText += value + "\n";
            }
        }

        public List<Feature> Features = new List<Feature>();

        public string FileLocation { get; private set; }

        public List<Item> Items = new List<Item>();

        public string Name { get; private set; }

        #endregion

        #region Privete Methods

        private void readConfig(string configText)
        {
            string[] lines = configText.Split('\n');

            string currentFeature = string.Empty;

            foreach(string s in lines)
            {
                if (s.Replace(" ", "").StartsWith("#", StringComparison.CurrentCulture) == false)
                {
                    if (s.Replace(" ", "").StartsWith("[", StringComparison.CurrentCulture) 
                        && s.Replace(" ", "").EndsWith("]", StringComparison.CurrentCulture))
                    {
                        currentFeature = s.ToLower().Replace(" ", "").Replace("[", "").Replace("]", "");
                        continue;
                    }

                    if (currentFeature == string.Empty)
                    {
                        readConfigValue(s);
                    }
                    else
                    {
                        readFeatureValue(currentFeature, s);
                    }
                }
            }
        }

        private void readConfigValue(string line)
        {
            string[] lineSplit = line.Split('=');

            if (lineSplit.Length > 0)
            {
                Item item = Item(lineSplit[0].ToLower().Replace(" ", ""));

                if (item != null && lineSplit.Length >= 2)
                {
                    string value = parseValue(lineSplit[1]);
                    item.SetValue(value);
                }
            }
        }

        private string parseValue(string line)
        {
            if (line.Replace(" ", "").StartsWith("{", StringComparison.CurrentCulture) 
                && line.Replace(" ", "").EndsWith("}", StringComparison.CurrentCulture))
            {
                line = line.Replace("{", "");
                line = line.Replace("}", "");
                string[] lineSplit = line.Split(',');
                line = "";

                for (int i = 0; i <= lineSplit.Length - 1; i++)
                {
                    line += (i < lineSplit.Length - 1) ? lineSplit[i] + "\n" : lineSplit[i];
                }

                return line;
            }
            else
            {
                return line;
            }
        }

        private void readFeatureValue(string featureName, string line)
        {
            Feature feature = Feature(featureName.ToLower().Replace(" ", "").Replace("[", "").Replace("]", ""));

            string[] lineSplit = line.Split('=');

            if (lineSplit.Length > 0 && feature != null)
            {
                Item item = feature.Item(lineSplit[0].ToLower().Replace(" ", ""));

                if (item != null && lineSplit.Length >= 2)
                {
                    string value = parseValue(lineSplit[1]);
                    item.SetValue(value);
                }
            }
        }

        private bool writeToFile(string fileLocation, string configText)
        {
            StreamWriter streamWriter = new StreamWriter(fileLocation + Name + ".cfg");

            try
            {
                streamWriter.WriteLine(configText);
            }
            catch(Exception ex)
            {
#if DEBUG
                Console.WriteLine("Config.writeToFile EX " + ex.Message);
#else
                throw ex;
#endif
            }
            finally
            {
                streamWriter.Close();
            }

            return true;
        }

        #endregion

        #region Public Methods

        public void AddFeature(Feature feature)
        {
            Features.Add(feature);
        }

        public void AddItem(Item item)
        {
            Items.Add(item);
        }

        public void ClearConfigText()
        {
            _configText = string.Empty;
        }

        public void Load(string directory)
        {
            string fileLocation = directory + Name + ".cfg";
            if (File.Exists(fileLocation))
            {
                StreamReader streamReader = new StreamReader(fileLocation);

                string fileText = "";

                try
                {
                    fileText = streamReader.ReadToEnd();
                }
                catch (Exception ex)
                {
#if DEBUG
                    Console.WriteLine("Config.writeToFile EX " + ex.Message);
                    ResetToDefault();
                    fileText = null;
#else
                    ResetToDefault();
                    fileText = null;
#endif
                }
                finally
                {
                    streamReader.Close();
                }

                if (fileText != null)
                {
                    readConfig(fileText);
                }
            }
            else
            {
                ResetToDefault();
            }

        }

        public bool Export(string directory)
        {
            _configText = "";

            ConfigText = $"#Config {Name}";

            foreach (Item item in Items)
            {
                ConfigText = item.GetExportText();
            }

            foreach (Feature feature in Features)
            {
                ConfigText = feature.GetExportText();
            }

            writeToFile(directory, ConfigText);

            return true;
        }

        public void PrintExport()
        {
            _configText = "";

            ConfigText = $"#Config {Name}";

            foreach (Item item in Items)
            {
                ConfigText = item.GetExportText();
            }

            foreach (Feature feature in Features)
            {
                ConfigText = feature.GetExportText();
            }

            Console.WriteLine(ConfigText);
        }

        public void ResetToDefault()
        {
            foreach (Item item in Items)
            {
                item.SetDefault();
            }

            foreach(Feature feature in Features)
            {
                feature.SetDefault();
            }
        }

        public Feature Feature(string featureName)
        {
            featureName = featureName.ToLower().Replace(" ", "");

            foreach (Feature feature in Features)
            {
                string currentFeatureName = feature.Name.ToLower().Replace(" ", ""); 
                if (currentFeatureName == featureName)
                {
                    return feature;
                }
            }

            return null;
        }

        public Item Item(string name)
        {
            name = name.ToLower().Replace(" ", "");

            foreach (Item item in Items)
            {
                string currentItemName = item.Name.ToLower().Replace(" ", "");

                if (currentItemName == name)
                {
                    return item;
                }
            }

            return null;
        }

        #endregion

        #region Constructors

        public Config(string name)
        {
            Name = name;
        }

        #endregion
    }
}
